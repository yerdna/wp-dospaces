#WP Offload Digital Ocean Spaces

Modification of "WP Offload S3 Lite" plugin to work with Digital Ocean Spaces.

This will work with buckets in AMS3 Digital Ocean Spaces region.

= Installation =

* Copy 'wp-offload-dospaces' folder to your WordPress plugins directory.
* Activate the plugin.
* Add 'tantan_wordpress_s3' option into your WordPress 'wp_options'
  (or 'yourwpprefix_options') table using tantan_wordpress_s3.txt as template,
  then replace values for 'aws-access-key-id', 'aws-secret-access-key', 'bucket'
  and 'cloudfront' with your Digital Ocean Spaces values
  (it's a serialized option so don't forget to check/change string length).
* Try to upload an image (WP Admin Media - Add New),
  it will be saved to your DO Spaces 'mybucket' bucket like:
  https://mybucket.ams3.digitaloceanspaces.com/wp-content/uploads/2018/04/myimage.jpg 
